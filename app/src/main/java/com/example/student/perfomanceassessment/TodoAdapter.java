package com.example.student.perfomanceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder>{
    private ArrayList<TodoPost> TodoPosts;

    public TodoAdapter(ActivityCallback activityCallback, ArrayList<TodoPost> TodoPosts ){
        this.TodoPosts = TodoPosts ;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        holder.titleText.setText(TodoPosts.get(position).title);
    }

    @Override
    public int getItemCount() {
        return TodoPosts.size();
    }
}