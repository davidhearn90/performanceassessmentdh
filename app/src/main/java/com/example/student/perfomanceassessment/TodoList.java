package com.example.student.perfomanceassessment;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoList extends Fragment {
    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
        this.activity = (MainActivity) activity;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        TodoPost i1 = new TodoPost("hi", "h", "d", "r");
        TodoPost i2 = new TodoPost("today", "tomorrow", "dh", "cksd");
        TodoPost i3 = new TodoPost("dfs", "dfs", "dsc", "dsf");
        TodoPost i4 = new TodoPost("dfda", "dsfa", "dfcads", "daf");

        activity.ToDoPost.add(i1);
        activity.ToDoPost.add(i2);
        activity.ToDoPost.add(i3);
        activity.ToDoPost.add(i4);


        TodoAdapter adapter = new TodoAdapter(activityCallback, activity.TodoPost);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
