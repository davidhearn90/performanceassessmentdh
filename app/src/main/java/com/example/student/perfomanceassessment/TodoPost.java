package com.example.student.perfomanceassessment;

public class TodoPost {
        public String title;
        public String date;
        public String due;
        public String work;

        public TodoPost(String title, String date,String due,String work){
            this.title = title;
            this.date = date;
            this.due = due;
            this.work = work;
        }
    }
