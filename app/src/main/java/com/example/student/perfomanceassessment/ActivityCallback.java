package com.example.student.perfomanceassessment;


import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}