package com.example.student.perfomanceassessment;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class TodoHolder extends RecyclerView.ViewHolder{
    public TextView titleText;

    public TodoHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}